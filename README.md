# Programming Quickstart

This document serves as a quick start to introduce some basic programming
concepts and terms that will get you up and running with writing your own
programs.

## This File

This is called a "readme" file and it is often placed in the top level
folder of a project. It is usually named "README", "README.md", or
"README.txt". You can open these files with any text editor to view them,
however the .md files are written in "Markdown" and are best viewed in
a Markdown viewer. Visual Studio Code with the Markdown extension installed
is usually a pretty good way to do this. Once you open the file, you can
press Ctrl+Shift+V to open the preview. If you are reading this in the raw
text form, you may want to switch over to that now.

Here are some sources for more information about Markdown:

* [Github Markdown Reference](https://docs.github.com/en/get-started/writing-on-github)
* [Wikipedia](https://en.wikipedia.org/wiki/Markdown)
* [Markdown Guide](https://www.markdownguide.org/)

## Version Control

"Version Control" is a fancy term just meaning a system for keeping track of
changes to a file or folder. It is an alternative to having a bunch of
"Book Report Final Final 3.docx" files. There are many different systems for
Version Control, the most common is called "Git", others include "Perforce",
"Mercurial", and "Apache Subversion (SVN)". Often you will here the project
stored on a version control system referred to as a "repository" or "repo".

Most of the time, version control systems are configured to store a copy of
the files on a server somewhere (both as a backup, and to collaborate with
other programmers). There are a few very popular websites that provide that
server side hosting. The three main ones are:

* [GitLab](https://www.gitlab.com)
* [GitHub](https://www.github.com)
* [Bitbucket](https://bitbucket.org/product)

In fact this folder (and this file) are stored on gitlab:
https://gitlab.com/namiller/code101/

(notice that when you open this project on gitlab it shows the README file
prominantly on the main page and renders in a nicely readable way).

You can read more about version control in general on [wikipedia](https://en.wikipedia.org/wiki/Version_control)

Generally version control systems have a few basic operations:

### Downloading a project

When you want to start out with a project that someone has already set up on
a server somewhere, Git calls this cloning `git clone`. If you find a
project somewhere on github or gitlab that you want to play around with this
is usually the place to start.

For instance on a system with git installed, this project can be downloaded
with the command:
```
git clone https://gitlab.com/namiller/code101.git
```

### Staging local changes that you want to make permanent

In Git this is done by "adding" the file to the staging list via the
`git add` command. This just tells your local version of the files that
they are part of a change you care about.

[read more here](https://www.atlassian.com/git/tutorials/saving-changes)

### Creating a new checkpoint

Once you have added the files to the staging list and are happy with the
changes you have made, you can create a sort of checkpoint for the new
version of the project that includes the modifications. This is done by
"committing" the changes. These commits usually should be self contained so
that the version of the project at any commit "works correctly" (this is
especially important if you are working with other people on the project).
These commits also generally come with a message describing the changes that
were made. This is done via the `git commit` command.

[read more here](https://www.atlassian.com/git/tutorials/saving-changes/git-commit)

You can view the list of commits for this project here:
https://gitlab.com/namiller/code101/-/commits/main

### Uploading the changes you have made to the server

Once you have committed your changes, you often want to upload them to the
server so that if your machine breaks you don't lose them, and so that
anybody that is also using your project can download the changes. Git calls
this "pushing" your changes to the server, and this can be done via the
`git push` command. Note that if the version on the server has changed since
you last downloaded you may need to sync first (next section).

[read more here](https://www.atlassian.com/git/tutorials/syncing/git-push)

NOTE: When you aren't the author of the project this is a little more complicated. This won't work for this project.

### Syncing to the latest files on the server

Conversely if you would like to download the changes that were made to the
server version (by someone else pushing their changes), you can do so by
"pulling" the version down from the server via the `git pull` command. If
you have changes locally when you pull, you may need to resovle the
conflicts (this is often called "merging" and can be helped by the
`git merge` command.

[read more here](https://www.atlassian.com/git/tutorials/syncing/git-pull)

NOTE: I will try to periodically make improvements to this project so you may want to periodically pull.

## Starter Projects

This contains some sub-folders that each hold a different type of starter
project. They are listed here roughly in order of difficulty, but with
enough effort it should be possible to do them in any order.

### Hello World

The very most basic programs in a few different language/toolchains. This
can be a good starting place to ensure you know how to run a program, as
well as a starting point if you want a very simple program to make
incremental changes to.

### Scripting

This is a small demonstration of how sometimes even very simple and small
programs can be useful (even if only ever run from the command line rather
than made into apps or websites).

### Numeric Puzzles

This includes some numerical style puzzles that can be solved by writing a
program. These have a wide range of difficulties - these are of the style of
[Project Euler](https://projecteuler.net/) which has an additional range of
many more difficult questions.

[INCOMPLETE]

### Visual Projects

These are some projects that create interesting visual products.

[NOT YET STARTED]

### Interactive Projects

These are interactive projects, including some basic games.

[NOT YET STARTED]

### Web Projects

These are some basic projects for getting started with web development.

[NOT YET STARTED]

### Data Structures/Algorithms

These are some problems and demonstrations of classic Data Structures and
Algorithms

[NOT YET STARTED]

### Microcontroller Projects

Simple projects for microcontrollers. Primarily the esp8266 or arduino.

[NOT YET STARTED]

### Optimization

Basic introduction to optimization problems.

[NOT YET STARTED]

### Machine Learning

Intro to machine learning theory and practice.

[NOT YET STARTED]

## Getting unstuck

If you ever get stuck working with the git version control system, you can
always start from scratch by cloning the repository again into a new folder and optionally deleting the old folder. This project can be downloaded to a
newly created folder named "folder\_name" with the following command:

```
git clone https://gitlab.com/namiller/code101.git folder_name
```

## General Advice

Programming is sometimes an excercise in frustration. There is probably no
way to avoid that, however frustration can be reduced by following some
"best practices."

### Reading documentation

`man`, `--help`.

### Incremental work

### Test driven development

### Debugging

## Overview of the toolchain

The "toolchain" is what we call the assortment of programs used to run
programs that we have written.

### Compiler

### Interpreter

### Package Manager

### Build system

TODO(namiller): Add a tag to all projects that require linux to really
experience.
