#!/bin/bash

# A TODO comment is a common way to indicate some work being left for the
# future. As such it is nice to be able to search through a whole project
# for all remaining todos.

grep -rni 'todo' ../.
