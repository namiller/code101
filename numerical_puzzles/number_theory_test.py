"""These tests are used to ensure the solutions are correct.

They will show which test cases are passing/which functions have been
correctly implemented when run.
"""

import unittest
import number_theory

class TestNumberTheory(unittest.TestCase):

  def test_gcd(self):
    self.assertEqual(number_theory.gcd(15,12), 3)
    self.assertEqual(number_theory.gcd(2**10, 500), 4)
    self.assertEqual(number_theory.gcd(24, 101), 1)
    self.assertEqual(number_theory.gcd(999, 24), 3)

  def test_nth_prime(self):
    self.assertEqual(number_theory.nth_prime(1), 2)
    self.assertEqual(number_theory.nth_prime(100), 541)
    self.assertEqual(number_theory.nth_prime(100_000), 1299709)


if __name__ == '__main__':
  unittest.main()

