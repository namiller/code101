# Numerical puzzles

Most of these can be solved without any fancy math, but often can be sped
up considerable with more sophisticated math and algorithms.

Each set of puzzles is self contained and comes with 3 files.

* puzzle\_type.py - this is where you can implement your solution.
* puzzle\_type\_test.py - this is how we check solutions.
* puzzle\_type\_solution.py - this is a reference solution.

Note that while each puzzle type is self contained, each problem within each
type may not be. The order in the file is likely the order you will want to
solve them.

## Checking your answers

Your solution can be verified by running the test. This can be done by
invoking:

```
python3 puzzle_type_test.py
```

It will indicate any failing puzzles, and provide some guidance as to what
the expected value vs returned value was.

## Puzzle Types

### Number Theory

### Probability/Counting

### Project Euler

These are a small selection of a few simple Project Euler puzzles.
