def gcd(n: int, m: int) -> int:
  """Computes the greatest common divisor between arguments."""
  # TODO(you): Implement.
  return 1

def nth_prime(n: int) -> int:
  """Calculates the nth largest prime number."""
  # TODO(you): Implement.
  return 0

def all_divisors(n: int) -> set[int]:
  """Computes all factors of n."""
  # TODO(you): Implement.
  return set()

def expand_polynomial(zeros: list[int]) -> list[int]:
  """Converts the factored polynomial into a list of coefficients.

  Args:
    zeros: The list of zeros of the polynomial. The function:
           y = (x+4)(x-2)(x-2)(x+3) would be: [-4, 2, 2, -3].
  Returns:
    The expanded polynomial as a list of coefficients in increasing order.
  """
  # TODO(you): Implement.
  return zeros

def factor_polynomial(coef: list[int]) -> list[int]:
  """Given a list of polynomial coefficients, return the zeros.

  This has the restriction that all zeros must be integers. If not an
  exception will be thrown

  Args:
    coef: The polynomial coefficients in increasing order. The polynomial
          y = 3x^3 + 5x + 1 would be represented as: [1, 5, 0, 3].
  Returns:
    The list of values of x for which y will take the value of 0. Repeat
    zeros will be repeated in the output so len(coef) - 1 == len(output).
  """
  # TODO(you): Implement.
  return coef;
