from itertools import count

def gcd(n: int, m: int) -> int:
  """Computes the greatest common divisor between arguments."""
  # Ensure that n < m.
  while n!=m and n*m !=0:
    # Ensure that n<m (swap if not).
    n,m = sorted((n,m))
    # Subtract the largest multiple of n we can from m.
    m -= n*(m//n)
  return max(n, m)

def nth_prime(n: int):
  sieve = [2, 3]
  while len(sieve) < n:
    for candidate in count(sieve[-1]+2, 2):
      is_prime = True
      for e in sieve:
        if candidate % e == 0:
          is_prime=False
          break
        if e*e > candidate:
          break
      if is_prime:
        sieve.append(candidate)
        break;
  return sieve[n-1]

def all_divisors(n: int) -> set[int]:
  """Computes all factors of n."""
  return {k for k in range(n) if n%k==0}

def expand_polynomial(zeros: list[int]) -> list[int]:
  """Converts the factored polynomial into a list of coefficients.

  Args:
    zeros: The list of zeros of the polynomial. The function:
           y = (x+4)(x-2)(x-2)(x+3) would be: [-4, 2, 2, -3].
  Returns:
    The expanded polynomial as a list of coefficients in increasing order.
  """
  def prod(values: list[int]):
    p = 1
    for e in values:
      p *= e
    return p
  coefs = [0] * (len(zeros)+1)
  for i in range(2**len(zeros)):
    mask = [(i >> k) & 1 for k in range(len(zeros))]
    coefs[sum(mask)] += prod([v for m,v in zip(mask, zeros) if not m])
  return zeros

def factor_polynomial(coef: list[int]) -> list[int]:
  """Given a list of polynomial coefficients, return the zeros.

  This has the restriction that all zeros must be integers. If not an
  exception will be thrown

  Args:
    coef: The polynomial coefficients in increasing order. The polynomial
          y = 3x^3 + 5x + 1 would be represented as: [1, 5, 0, 3].
  Returns:
    The list of values of x for which y will take the value of 0. Repeat
    zeros will be repeated in the output so len(coef) - 1 == len(output).
  """
  # By the rational root theorem we just need to factor coef[0], coef[-1],
  # Then check for zeros, carry out long division, then recurse.
  # TODO(namiller): Implement.
  return coef;
