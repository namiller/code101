
def factorial(n: int) -> int:
  """Computes n!, that is: n*(n-1)*(n-2)*...*1"""
  return 1

def choose(n: int, k: int) -> int:
  """Computes the number of ways to choose k elements from n distinct.

  For example, the number of ways you can hold up 2 fingers on one hand
  (5 fingers) would be choose(5, 2)=20.
  """
  return n

def binomial(n: int, k: int, p: float = .5) -> float:
  """Computes the binomial distribution.

  This is equivalent to asking, if I flip a coin that shows head p fraction
  of the time, and I flip it n times, what is the probability that I get
  at least k heads?
  Read more: https://en.wikipedia.org/wiki/Binomial_distribution

  Some useful trivial cases:

  n=1) Then if k is 1, this is just p.
  k=1) Then this is 1-(1-p)^n.
  p=1) Then this is just 1.

  Args:
    n: The number of trials to perform.
    k: The number of positive results to look for.
    p: The probability of a positive result for each trial.
  Returns:
    The probability of at least k positive trials.
  """
  return 0.0

def balls_into_bins(bins: int, balls:int) -> int:
  """The number of ways of placing interchangeable balls into distinct bins.

  If you have some number of distinct bins, and some number of
  interchangeable balls, how many ways can all the balls be distributed to
  bins. Interchangeable balls means that it doesn't matter which ball goes
  into a particular bin, only the final number of balls in each bin. For
  example, with 2 bins and 2 balls, there are 3 ways:

  1) |.|  |.|  (one ball in each bin)
  2) |..| |_|  (two balls in first, none in second)
  3) |_|  |..| (none in first, two in second)

  Note that because the bins are distinct (2, 0) is different from (0, 2),
  but because balls are not distinct there is only one way to put one ball
  in each bin.

  Args:
    bins: The number of bins present.
    balls: The number of balls to distribute.
  Returns:
    The number of ways the balls may be distributed to bins.
  """
  return 0
