# Hello World

This is the standard first program one should write. You'll find that most
tutorials and guides start with hello world in some way or form. As to not
break from tradition we have a few examples of hello world programs here.
The hope is that this provides pretty good coverage of the various
languages/toolchains used in other projects. It is always a good idea to
make sure you can run these before trying something more complicated. If
your system is not correctly configured you will be unable to run these and
there is less that can go wrong.

## Languages

### Python (.py)

Python is interpreted and thus does not need to be compiled. It can be run
directly with:

```
python3 hello.py
```

### Python Notebook (.ipynb)

These interactive python notebooks area really handy for quickly iterating
on ideas in python. They can be run using jupyter and the browswer:

TODO(namiller): Finish.

### C++ (.cc/.cpp,.h)

The minimal toolchain for c++ is to use g++ to compile into a runable
binary:

```
g++ hello.cc
```

This will either output an error message (compiler error) or produce a
runnable file. By default this will be called `a.out` and can be run by
simply typing `./a.out`

### Java (.java)

The minimal toolchain for java is to use javac to compile and java to run:

```
javac hello.java
```

This will either output an error message or produce the file
HelloWorld.class which can then be run with:

```
java HelloWorld
```

### BASH (.sh)

This can be run directly from the command line as if it were an executable
binary like:

```
./hello.sh
```

Note that if you are making your own bash file to run, by default files are
not executable and you need to tell the compute that it is ok to try and run
this file. This can be done with the command

```
chmod +x your_file.sh
```

This only needs to be done once after you create the file.

### Javascript (.js)

There are a few different ways of running javascript. Traditionally it is
only by the browser when using the internet, however more recently it has
taken off for local execution via NodeJs. We can directly run a javascript
file using NodeJs via:

```
node hello.js
```

### HTML (.html)

The most natural way to view the file is to open it in a web browswer. This
can be done by either dragging and dropping the file into the browswer or
just double clicking it.

Note that this may not work as expected if you are using additional
resources (javascript/css) which need to be served. See hello\_web for a
more complete example.

### Multi File Examples

One big jump while learning to program is the jump from single file, to
multiple files. Here are some examples for how the tooling changes for
multi-file projects. Each will have it's own instructions for running in
it's own readme.

* hello\_bazel
* hello\_web
* hello\_make
